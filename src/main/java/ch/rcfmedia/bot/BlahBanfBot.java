package ch.rcfmedia.bot;

import ch.rcfmedia.bot.handlers.ICallbackHandler;
import ch.rcfmedia.bot.handlers.IMessageHandler;
import ch.rcfmedia.bot.persistence.Persistence;
import ch.rcfmedia.bot.statistics.StatisticsInputRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.telegram.telegrambots.api.methods.BotApiMethod;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static java.util.stream.Collectors.joining;

@Singleton
public class BlahBanfBot extends TelegramLongPollingBot implements IMessageDispatcher {

	private static final Log log = LogFactory.getLog(BlahBanfBot.class);

	@Inject
	@Named("botName")
	String botName;
	@Inject
	Persistence persistence;
	@Inject
	BlahBanfConfig config;

	private Set<ICallbackHandler> callbackHandlers;
	private Set<IMessageHandler> messageHandlers;

	@Inject
	public BlahBanfBot(Set<IMessageHandler> messageHandlers, Set<ICallbackHandler> callbackHandlers) {
		this.messageHandlers = Collections.unmodifiableSet(messageHandlers);
		log.info(String.format("Installed %d message handlers: %s", messageHandlers.size(), toString(messageHandlers)));
		this.callbackHandlers = Collections.unmodifiableSet(callbackHandlers);
		log.info(String.format("Installed %d callback handlers: %s", callbackHandlers.size(),
				toString(callbackHandlers)));
	}

	public void onUpdateReceived(Update update) {
		if (isCallback(update)) {
			handleCallback(update.getCallbackQuery());
		}
		if (isMessage(update)) {
			handleMessage(update.getMessage());
		}
	}

	private boolean isCallback(Update update) {
		return update.hasCallbackQuery();
	}

	private boolean isMessage(Update update) {
		Message message = update.getMessage();
		if (message == null) {
			return false;
		}
		// ignore empty messages
		if (!message.hasText()) {
			return false;
		}
		// ignore bots
		User from = message.getFrom();
		if (from.getBot()) {
			return false;
		}

		return true;
	}

	private void handleCallback(CallbackQuery callbackQuery) {
		callbackHandlers.stream().filter(h -> h.canHandle(callbackQuery)).forEach(h -> h.handle(callbackQuery));
	}

	private void handleMessage(Message message) {
		messageHandlers.stream().filter(h -> h.canHandle(message)).forEach(h -> h.handle(message));
		// update stats
		if (!message.isCommand()) {
			StatisticsInputRow statisticUpdateRow = new StatisticsInputRow(message);
			persistence.updateStats(statisticUpdateRow);
		}
	}

	public void doSendMessage(BotApiMethod<?> message, String handlerIdent) {
		try {
			sendApiMethod(message);
		} catch (TelegramApiException e) {
			log.error(String.format("handler %s error", handlerIdent), e);
		}
	}

	private <T> String toString(Collection<T> coll) {
		return coll.stream().map(Object::getClass).map(Class::getSimpleName).collect(joining(",", "[", "]"));
	}

	public String getBotUsername() {
		return botName;
	}

	public String getBotToken() {
		return config.getBotToken();
	}

}

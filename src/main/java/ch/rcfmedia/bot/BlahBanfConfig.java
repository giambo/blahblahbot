package ch.rcfmedia.bot;

import com.google.common.base.Strings;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Singleton;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Singleton
public class BlahBanfConfig {

	private static final Log log = LogFactory.getLog(BlahBanfConfig.class);

	private Properties config;

	public BlahBanfConfig() throws Exception {
		InputStream is = BlahBanfBot.class.getResourceAsStream("/config.properties");
		if (is == null) {
			log.fatal("Cannot find config file 'config.properties'");
			throw new FileNotFoundException("Cannot find config file 'config.properties'");
		}
		config = new Properties();
		try {
			config.load(is);
		} catch (IOException e) {
			log.fatal("Cannot read config file 'config.properties'");
			throw e;
		}
	}

	String getBotToken() {
		return getProperty("bot.token");
	}

	public String getHostName() {
		return getProperty("mysql.hostName");
	}

	public String getUserName() {
		return getProperty("mysql.userName");
	}

	public String getPassword() {
		return getProperty("mysql.password");
	}

	private String getProperty(String propertyKey) {
		String propertyValue = config.getProperty(propertyKey);
		if (Strings.isNullOrEmpty(propertyValue)) {
			String msg = String.format("Cannot read property '%s' from config", propertyKey);
			log.fatal(msg);
			throw new IllegalArgumentException(msg);
		}
		return propertyValue;
	}

}

package ch.rcfmedia.bot;

import org.telegram.telegrambots.api.methods.BotApiMethod;

public interface IMessageDispatcher {

	default void doSendMessage(BotApiMethod message, Class callerClass) {
		doSendMessage(message, callerClass.getSimpleName());
	}

	void doSendMessage(BotApiMethod<?> message, String callerIdent);

}

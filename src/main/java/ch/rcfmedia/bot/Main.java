package ch.rcfmedia.bot;

import ch.rcfmedia.bot.config.Configurator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;

import java.time.LocalDateTime;

public class Main {

	private static final Log log = LogFactory.getLog(Main.class);

	public static void main(String[] args) {
		try {
			log.info("Started at " + LocalDateTime.now());
			ApiContextInitializer.init();
			BlahBanfBot blahBanfBot = Configurator.doConfig();
			new TelegramBotsApi().registerBot(blahBanfBot);
		} catch (Exception e) {
			log.fatal(e);
		}
	}
}

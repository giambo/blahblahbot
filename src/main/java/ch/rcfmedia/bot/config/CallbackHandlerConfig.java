package ch.rcfmedia.bot.config;

import ch.rcfmedia.bot.handlers.ICallbackHandler;
import ch.rcfmedia.bot.handlers.callback.StatisticCallbackHandler;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

class CallbackHandlerConfig extends AbstractModule {

	@Override
	protected void configure() {
		Multibinder<ICallbackHandler> multibinder = Multibinder.newSetBinder(binder(), ICallbackHandler.class);
		multibinder.addBinding().to(StatisticCallbackHandler.class);
	}
}

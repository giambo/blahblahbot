package ch.rcfmedia.bot.config;

import ch.rcfmedia.bot.BlahBanfBot;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;

public class Configurator {

	public static BlahBanfBot doConfig() {
		Injector injector = Guice.createInjector(Stage.PRODUCTION,
				new MiscConfig(),
				new StringsConfig(),
				new MessageHandlerConfig(),
				new CallbackHandlerConfig()
		);
		return injector.getInstance(BlahBanfBot.class);
	}
}

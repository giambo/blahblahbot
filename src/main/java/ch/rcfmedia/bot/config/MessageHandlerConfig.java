package ch.rcfmedia.bot.config;

import ch.rcfmedia.bot.handlers.BlahBanfMentionHandler;
import ch.rcfmedia.bot.handlers.IMessageHandler;
import ch.rcfmedia.bot.handlers.command.*;
import ch.rcfmedia.bot.handlers.command.kamasutra.KamasutraCommandHandler;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

class MessageHandlerConfig extends AbstractModule {

	@Override
	protected void configure() {
		Multibinder<IMessageHandler> multibinder = Multibinder.newSetBinder(binder(), IMessageHandler.class);
		multibinder.addBinding().to(BlahBanfMentionHandler.class);
		multibinder.addBinding().to(StartCommandHandler.class);
		multibinder.addBinding().to(StatisticCommandHandler.class);
		multibinder.addBinding().to(HoroscopeCommandHandler.class);
		multibinder.addBinding().to(KamasutraCommandHandler.class);
		multibinder.addBinding().to(BlasphemyCommandHandler.class);
		multibinder.addBinding().to(BitcoinCommandHandler.class);
		multibinder.addBinding().to(EthereumCommandHandler.class);
	}
}

package ch.rcfmedia.bot.config;

import ch.rcfmedia.bot.BlahBanfBot;
import ch.rcfmedia.bot.IMessageDispatcher;
import ch.rcfmedia.bot.handlers.command.CommandLineParser;
import ch.rcfmedia.bot.persistence.BlahBanfDataSource;
import ch.rcfmedia.bot.persistence.Persistence;
import ch.rcfmedia.bot.persistence.Statistics;
import com.google.inject.AbstractModule;

import javax.sql.DataSource;

class MiscConfig extends AbstractModule {

	@Override
	protected void configure() {
		bind(BlahBanfBot.class);
		bind(IMessageDispatcher.class).to(BlahBanfBot.class);
		bind(Persistence.class);
		bind(DataSource.class).to(BlahBanfDataSource.class);
		bind(Statistics.class);
		bind(CommandLineParser.class);
	}
}

package ch.rcfmedia.bot.config;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

class StringsConfig extends AbstractModule {

	@Override
	protected void configure() {
		bind(String.class).annotatedWith(Names.named("botName")).toInstance("BlahBanfBot");
		bind(String.class).annotatedWith(Names.named("startCommand")).toInstance("/start");
		bind(String.class).annotatedWith(Names.named("statsCommand")).toInstance("/stats");
		bind(String.class).annotatedWith(Names.named("horoscopeCommand")).toInstance(("/oroscopo"));
		bind(String.class).annotatedWith(Names.named("kamasutraCommand")).toInstance(("/kamasutra"));
		bind(String.class).annotatedWith(Names.named("blasphemyCommand")).toInstance(("/bestemmia"));
		bind(String.class).annotatedWith(Names.named("btcCommand")).toInstance("/btc");
		bind(String.class).annotatedWith(Names.named("ethCommand")).toInstance("/eth");
	}
}

package ch.rcfmedia.bot.handlers;

import com.google.common.base.Strings;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.EntityType;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.MessageEntity;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Random;

import ch.rcfmedia.bot.IMessageDispatcher;

@Singleton
public class BlahBanfMentionHandler implements IMessageHandler {

	private static final String[] CONFUSED_ANSWERS = new String[]{
			"Ti vedo consufo, scrivi /start per disconsufonarti.",
			"Eh ? Non capisco ...",
			"Pretendi troppo da me, in fondo sono una creatura di Giambo !",
			"Adesso arriva la Suora e te lo spiega.",
			"Hei, ma questo non e' il FdT !",
			"Confucio una volta disse '_Questo bot fa schifo !_'"
	};

	@Inject
	private DataSource dataSource;

	@Inject
	private IMessageDispatcher messageDispatcher;

	@Inject
	@Named("botName")
	String botName;

	private Random randomGen;

	public BlahBanfMentionHandler() {
		randomGen = new Random();
	}

	@Override
	public boolean canHandle(Message message) {
		if (message.getEntities() == null) {
			return false;
		}
		String mention = message.getEntities().stream()
				.filter(Objects::nonNull)
				.filter(e -> e.getOffset() == 0)
				.filter(e -> EntityType.MENTION.equals(e.getType()))
				.map(MessageEntity::getText)
				.findFirst()
				.orElse(null);

		if (Strings.isNullOrEmpty(mention)) {
			return false;
		}

		return mention.equals("@" + botName);
	}

	@Override
	public void handle(Message message) {
		String text = message.getText();
		String sendMessageText;
		if (text.toUpperCase().contains("COME STAI")) {
			sendMessageText = doDiagnose();
		} else {
			sendMessageText = CONFUSED_ANSWERS[randomGen.nextInt(CONFUSED_ANSWERS.length)];
		}

		SendMessage sendMessage = new SendMessage(message.getChatId(), sendMessageText);
		sendMessage.enableMarkdown(true);
		messageDispatcher.doSendMessage(sendMessage, this.getClass());
	}

	private String doDiagnose() {
		try (Connection ignored = dataSource.getConnection()) {
			return "Io sto bene grazie, e tu ?";
		} catch (SQLException e) {
			return "Lamento un dolore al database ...";
		}
	}
}

package ch.rcfmedia.bot.handlers;

import org.telegram.telegrambots.api.objects.CallbackQuery;

public interface ICallbackHandler {

	boolean canHandle(CallbackQuery callbackQuery);

	void handle(CallbackQuery callbackQuery);
}

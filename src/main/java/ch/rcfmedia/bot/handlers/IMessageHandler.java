package ch.rcfmedia.bot.handlers;

import org.telegram.telegrambots.api.objects.Message;

public interface IMessageHandler {

	 boolean canHandle(Message message);

	void handle(Message message);
}

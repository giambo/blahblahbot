package ch.rcfmedia.bot.handlers.callback;

import ch.rcfmedia.bot.IMessageDispatcher;
import ch.rcfmedia.bot.handlers.ICallbackHandler;
import ch.rcfmedia.bot.persistence.Persistence;
import ch.rcfmedia.bot.statistics.StatisticInputParser;
import ch.rcfmedia.bot.statistics.StatisticOutputRow;
import ch.rcfmedia.bot.statistics.StatisticPeriod;
import com.google.common.base.Strings;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.CallbackQuery;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class StatisticCallbackHandler implements ICallbackHandler {

	private static final String COMMAND = "/stats";

	@Inject
	Persistence persistence;

	@Inject
	IMessageDispatcher messageDispatcher;

	@Override
	public boolean canHandle(CallbackQuery callbackQuery) {
		String inputText = callbackQuery.getData();
		if (Strings.isNullOrEmpty(inputText)) {
			return false;
		}
		return inputText.trim().toLowerCase().startsWith(COMMAND);
	}

	@Override
	public void handle(CallbackQuery callbackQuery) {

		String callbackData = callbackQuery.getData();
		StatisticPeriod statisticPeriod = StatisticInputParser.getStatsPeriod(callbackData);
		Long chatId = callbackQuery.getMessage().getChatId();
		List<StatisticOutputRow> stats = persistence.getStats(chatId, statisticPeriod);

		String statsFormatted = stats.stream()
				.map(StatisticOutputRow::getStatRow)
				.collect(Collectors.joining("\n"));

		String period;
		switch (statisticPeriod) {
			case ONE_DAY:
				period = "nelle ultime 24 ore";
				break;
			case ONE_HOUR:
				period = "nell'ultima ora";
				break;
			case BEGIN:
				period = "da quando me lo ricordo";
				break;
			default:
				period = "";
		}
		String answer = String.format("Numero di messaggi %s:\n%s", period, statsFormatted);

		Integer messageId = callbackQuery.getMessage().getMessageId();
		EditMessageText message = new EditMessageText()
				.setChatId(chatId)
				.setMessageId(messageId)
				.setText(answer)
				.enableMarkdown(true);
		
		messageDispatcher.doSendMessage(message, this.getClass());
	}
}

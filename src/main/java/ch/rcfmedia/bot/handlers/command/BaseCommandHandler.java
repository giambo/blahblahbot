package ch.rcfmedia.bot.handlers.command;

import ch.rcfmedia.bot.IMessageDispatcher;
import ch.rcfmedia.bot.handlers.IMessageHandler;
import com.google.common.base.Strings;
import org.telegram.telegrambots.api.objects.Message;

import javax.inject.Inject;

public abstract class BaseCommandHandler implements IMessageHandler {

	private String command;

	@Inject
	CommandLineParser commandLineParser;

	@Inject
	protected IMessageDispatcher messageDispatcher;

	protected BaseCommandHandler(String command) {
		this.command = command;
	}

	public abstract void handle(CommandLine commandLine);

	@Override
	public boolean canHandle(Message message) {
		if (!message.isCommand()) {
			return false;
		}
		CommandLine commandLine = getCommandLine(message);
		return canHandle(commandLine);
	}

	@Override
	public void handle(Message message) {
		if (!message.isCommand()) {
			return;
		}
		CommandLine commandLine = getCommandLine(message);
		handle(commandLine);
	}

	public boolean canHandle(CommandLine commandLine) {
		if (Strings.isNullOrEmpty(commandLine.getCommand())) {
			return false;
		}
		return command.equalsIgnoreCase(commandLine.getCommand());
	}

	private CommandLine getCommandLine(Message message) {
		String inputText = message.getText();
		Long chatId = message.getChatId();
		return commandLineParser.parse(inputText, chatId);

	}

}
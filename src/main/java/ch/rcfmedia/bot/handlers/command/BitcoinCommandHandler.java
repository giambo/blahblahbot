package ch.rcfmedia.bot.handlers.command;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
public class BitcoinCommandHandler extends CurrencyCommandHandler {

	@Inject
	BitcoinCommandHandler(@Named("btcCommand") String command) {
		super(command);
	}

	@Override
	String getFromCurrency() {
		return "BTC";
	}

}

package ch.rcfmedia.bot.handlers.command;

import com.google.common.collect.ImmutableList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;
import java.util.Random;

@Singleton
public class BlasphemyCommandHandler extends BaseCommandHandler {

	private static final Random rand = new Random();

	private static final List<String> part1 = ImmutableList.<String>builder().add(
		"dio",
		"gesu'",
		"cristo",
		"padre pio",
		"san gennaro",
		"lo spirito santo",
		"il padre nostro",
		"il papa",
		"don matteo",
		"matteo montesi",
		"papa razzinger",
		"mose'",
		"noe'",
		"adamo",
		"matusalemme",
		"abramo",
		"isacco",
		"giacobbe",
		"salomone",
		"gaspare",
		"melchiorre",
		"baldassarre"
	).build();

	private static final List<String> part2 = ImmutableList.<String>builder().add(
		"fottuto",
		"schifoso",
		"disgustoso",
		"ripugnante",
		"rivoltante",
		"spregevole",
		"viscido",
		"scivoloso",
		"mitologico",
		"banale",
		"insignificante",
		"falso",
		"ingannevole",
		"finto",
		"lurido",
		"pessimo",
		"immondo",
		"infame",
		"laido",
		"merdoso",
		"scandaloso",
		"vergognoso",
		"sfacciato",
		"smisurato",
		"odioso",
		"maledetto",
		"abominevole",
		"repellente",
		"lassativo",
		"matematico",
		"preciso",
		"infallibile",
		"fallito",
		"impanato",
		"mega",
		"ridicolo",
		"sfollato",
		"condannato",
		"riconosciuto",
		"rinomato",
		"ricercato",
		"inimmaginabile",
		"straordinario",
		"strepitoso",
		"fantastico",
		"fantasmagorico",
		"aberrante",
		"brutto",
		"maledetto",
		"antiestetico",
		"sgraziato",
		"disgraziato",
		"sconcio",
		"sfigato",
		"sudicio",
		"sventrato",
		"scemonito",
		"rigoroso",
		"rosicante",
		"roboante",
		"gracile",
		"aitante",
		"arguto",
		"squallido",
		"povero",
		"miserabile",
		"triste",
		"sporco",
		"lercio",
		"zozzo",
		"lordo",
		"spregevole",
		"ignobile",
		"disprezzabile",
		"immondo",
		"dannato",
		"funesto",
		"tremendo",
		"immenso",
		"grande"
	).build();

	private static final List<String> part3 = ImmutableList.<String>builder().add(
		"bimbominkia",
		"selfieminkia",
		"panettiere",
		"negro",
		"necrofilo",
		"porcellino d'india",
		"balbulziente",
		"spazzino",
		"lavacessi",
		"pezzo di stronzo",
		"friulano",
		"ingegnere",
		"mickey mouse",
		"illuminato",
		"scaldacollo",
		"interista",
		"lebbroso",
		"pelato",
		"colon",
		"liquido staminale",
		"liquido seminale",
		"marongelli",
		"Luca Liponi",
		"jim gam",
		"indiano",
		"marrone",
		"micro organismo",
		"sandwich",
		"immigrato",
		"genovese",
		"napoletano",
		"peruviano",
		"comunista",
		"barbone",
		"tumore",
		"cane mutilato",
		"ebreo",
		"pilota canadese",
		"nativo americano",
		"carpaccio",
		"transessuale",
		"nano",
		"paolella",
		"ciccione",
		"obeso",
		"lardone",
		"trippone",
		"barboncino",
		"profugo",
		"zingaro",
		"senzatetto",
		"rumeno",
		"albanese",
		"marocchino",
		"nigeriano",
		"sudafricano",
		"africano",
		"cinese",
		"pakistano",
		"cilanghese",
		"indonesiano",
		"americano",
		"messicano",
		"honduregno",
		"canturino",
		"scorreggione",
		"pezzente",
		"scarafaggio",
		"piccione",
		"gianni morandi",
		"extraterrestre",
		"extracomunitario",
		"john titor",
		"aquilotto75",
		"uruguayano",
		"marchigiano",
		"barbagianni",
		"buddista",
		"campo di grano",
		"devi jones",
		"yoshimitsu",
		"boomerang",
		"tostapane",
		"francese",
		"bersaglio dei bracconieri",
		"roditore",
		"smegma",
		"airbag",
		"australopiteco",
		"rettiliano",
		"terrapianista",
		"rabbino",
		"vespasiano",
		"disabile",
		"invalido",
		"depurificatore"
	).build();

	private static final List<String> part4 = ImmutableList.<String>builder().add(
		"col morgellon",
		"leggendario",
		"col culo aperto",
		"surgelato",
		"osteoporoso",
		"radioattivo",
		"col parkinson",
		"in offerta",
		"digievoluto",
		"sul golden gate",
		"in fila al supermercato",
		"nelle tubature",
		"baygonizzato",
		"su scherzi a parte",
		"sotto la neve",
		"a pescara del tronto",
		"sorpreso dall'imodium",
		"da campobasso",
		"satellitare",
		"primordiale",
		"abbattuto",
		"in mezzo ai preti",
		"lubrificato",
		"sordomuto",
		"sordocieco",
		"in onda su 7 gold alle 2 di notte",
		"diabetico",
		"siero positivo",
		"con watsapp scaduto",
		"autistico",
		"integratore",
		"integrale",
		"integrato",
		"cancerogeno",
		"corrosivo",
		"hi/lo",
		"vegano",
		"vegetariano",
		"inorganico",
		"che si scalda con la propria piscia",
		"in decomposizione",
		"buggato",
		"andrologo",
		"ispiratore di cesare ragazzi",
		"pubblicitario",
		"analogico",
		"esponenziale",
		"amico dei poveri",
		"che fa l'elemosina",
		"sotto i ponti",
		"mannaro",
		"economico",
		"impolverato",
		"castrato",
		"fake",
		"simulato",
		"inopportuno",
		"brufoloso",
		"nei peggiori bar di caracas",
		"nei bassifondi di quarto oggiaro",
		"pezzentone",
		"insaponato",
		"amareggiato",
		"sofferente",
		"agonizzante",
		"moribondo",
		"deformato",
		"vaccinato",
		"impiccato",
		"con la diarrea",
		"senza patente",
		"quasi umano",
		"extracomunitario",
		"troppo biondo",
		"concimato",
		"fertilizzato",
		"vivisezionato",
		"overclockkato",
		"disoccupato",
		"in cassa integrazione",
		"in chemioterapia",
		"sul forum dei brutti",
		"qualificato",
		"polisessuale",
		"monocromatico",
		"ergonomico",
		"multifunzionale",
		"con le crocs",
		"con le espadrillas",
		"con gli occhiali",
		"che mangia i fagioli",
		"che beve dai rubinetti",
		"con le ruote",
		"con la tubercolosi",
		"con tutte le epatiti",
		"che vive tra gli scarafaggi",
		"allergico alla carta",
		"allergico al nichel",
		"decontestualizzato",
		"vittima del bullismo",
		"in attesa per la tac",
		"terremotato",
		"che aspetta l'autobus nei giorni di sciopero",
		"uscito da un buco nero",
		"scartato dal carcere",
		"preso di mira dai prof",
		"in vacanza premio a chernobyl",
		"inviato speciale a bagdad",
		"in aereo con bin laden",
		"in cella di fianco a rosa bazzi",
		"a una grigliata in camper con paul walker",
		"al grande fratello mentre cade un meteorite",
		"che compra preservativi XXXL per fare il figo con la farmacista",
		"che esce di casa dimenticando di aver messo su l'acqua per la pasta",
		"che accetta i braccialetti portafortuna dei negri e poi non gli da soldi",
		"in tribunale contro geova",
		"paraplegico",
		"membro dei tassorosso",
		"ginecologo di uomini"
	).build();

	@Inject
	BlasphemyCommandHandler(@Named("blasphemyCommand") String command) {
		super(command);
	}

	@Override
	public void handle(CommandLine commandLine) {

		String response =
				part1.get(rand.nextInt(part1.size())) +
				" " +
				part2.get(rand.nextInt(part2.size())) +
				" " +
				part3.get(rand.nextInt(part3.size())) +
				" " +
				part4.get(rand.nextInt(part4.size()));

		SendMessage sendMessage = new SendMessage(commandLine.getChatId(), response);
		sendMessage.enableMarkdown(true);

		messageDispatcher.doSendMessage(sendMessage, this.getClass());
	}


}

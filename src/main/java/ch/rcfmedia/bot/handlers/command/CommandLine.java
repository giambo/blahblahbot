package ch.rcfmedia.bot.handlers.command;

public class CommandLine {

	private String command;
	private String[] arguments;
	private Long chatId;

	CommandLine() {
		this(null, null, null);
	}

	CommandLine(String command, String[] arguments, Long chatId) {
		this.command = command;
		this.arguments = arguments;
		this.chatId = chatId;
	}

	public String getFirstArgument() {
		if (arguments == null || arguments.length == 0) {
			return null;
		}
		return arguments[0];
	}

	public String getSecondArgument() {
		if (arguments == null || arguments.length < 1) {
			return null;
		}
		return arguments[1];
	}

	public String getCommand() {
		return command;
	}

	public String[] getArguments() {
		return arguments;
	}

	public Long getChatId() {
		return chatId;
	}
}

package ch.rcfmedia.bot.handlers.command;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Arrays;

@Singleton
public class CommandLineParser {

	@Inject
	@Named("botName")
	String botName;

	public CommandLine parse(String inputText, Long chatId) {
		if (Strings.isNullOrEmpty(inputText)) {
			return new CommandLine();
		}
		inputText = inputText.trim();
		if (!inputText.startsWith("/")) {
			return new CommandLine();
		}

		String command = setCommand(inputText);
		String[] arguments = setArguments(inputText);

		return new CommandLine(command, arguments, chatId);
	}

	private String setCommand(String inputText) {
		String[] tokens = inputText.split("\\s+");
		String[] commandPart = tokens[0].split("@");
		if (commandPart.length == 2 && commandPart[1].equalsIgnoreCase(botName)) {
			return commandPart[0];
		} else {
			return tokens[0];
		}
	}

	private String[] setArguments(String inputText) {
		String[] tokens = inputText.split("\\s+");
		if (tokens.length > 1) {
			return Arrays.copyOfRange(tokens, 1, tokens.length);
		}
		return null;
	}
}

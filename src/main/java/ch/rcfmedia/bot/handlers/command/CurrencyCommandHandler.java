package ch.rcfmedia.bot.handlers.command;

import com.google.common.base.Strings;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.telegram.telegrambots.api.methods.send.SendMessage;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Currency;
import java.util.concurrent.TimeUnit;

abstract class CurrencyCommandHandler extends BaseCommandHandler {

	private static final Log log = LogFactory.getLog(CurrencyCommandHandler.class);
	private static final String API_URL = "https://min-api.cryptocompare.com/data/price?fsym=%s&tsyms=%s";

	private final CloseableHttpClient httpclient;

	CurrencyCommandHandler(String command) {
		super(command);
		httpclient = HttpClientBuilder.create().setSSLHostnameVerifier(new NoopHostnameVerifier())
				.setConnectionTimeToLive(70, TimeUnit.SECONDS).setMaxConnTotal(10).build();
	}

	abstract String getFromCurrency();

	@Override
	public void handle(CommandLine commandLine) {
		Currency currency = getToCurrency(commandLine.getFirstArgument());
		if (currency == null) {
			SendMessage sendMessage = wrongCurrencySelected(commandLine.getChatId());
			messageDispatcher.doSendMessage(sendMessage, this.getClass());
			return;
		}

		Double value = getValueFor(currency);

		String messageText;
		String toCurrency = currency.getCurrencyCode();
		if (value != null) {
			messageText = String.format("Un %s vale %s %s", getFromCurrency(), value, toCurrency);
		} else {
			messageText = String.format("Impossibile trovare il valore di %s in %s", getFromCurrency(), toCurrency);
		}

		SendMessage sendMessage = new SendMessage(commandLine.getChatId(), messageText);
		sendMessage.enableMarkdown(true);

		messageDispatcher.doSendMessage(sendMessage, this.getClass());
	}

	private Double getValueFor(Currency currency) {
		String responseContent = fetchValueAsJson(currency);
		if (Strings.isNullOrEmpty(responseContent)) {
			String msg = "Cannot read conversion of %s -> %s";
			log.error(String.format(msg, getFromCurrency(), currency.getCurrencyCode()));
			return null;
		}
		JSONObject json = new JSONObject(responseContent);
		return json.getDouble(currency.getCurrencyCode());
	}

	private Currency getToCurrency(String currencyCode) {
		try {
			return Currency.getInstance(currencyCode.trim().toUpperCase());
		} catch (IllegalArgumentException e) {
			// just ignore
			return null;
		}
	}

	private String fetchValueAsJson(Currency currency) {
		String url = String.format(API_URL, getFromCurrency(), currency.getCurrencyCode());
		HttpGet httpGet = new HttpGet(url);
		try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
			BufferedHttpEntity httpEntity = new BufferedHttpEntity(response.getEntity());
			return EntityUtils.toString(httpEntity, StandardCharsets.UTF_8);
		} catch (IOException e) {
			log.error(e);
		}
		return null;
	}

	private SendMessage wrongCurrencySelected(Long chatId) {
		String messageText = "Valuta sconosciuta. Prova con *USD* oppure *EUR*";
		SendMessage sendMessage = new SendMessage(chatId, messageText);
		sendMessage.enableMarkdown(true);
		return sendMessage;
	}
}

package ch.rcfmedia.bot.handlers.command;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
public class EthereumCommandHandler extends CurrencyCommandHandler {

	@Inject
	EthereumCommandHandler(@Named("ethCommand") String command) {
		super(command);
	}

	@Override
	String getFromCurrency() {
		return "ETH";
	}
}

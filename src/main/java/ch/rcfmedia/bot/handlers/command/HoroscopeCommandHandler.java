package ch.rcfmedia.bot.handlers.command;

import com.google.common.collect.ImmutableList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;
import java.util.Random;

@Singleton
public class HoroscopeCommandHandler extends BaseCommandHandler {

	private static final Random rand = new Random();

	private static final List<String> part1 = ImmutableList.<String>builder().add(
		"Resi battaglieri da Marte che transita nel segno",
		"Incalzati da Giove",
		"Incazzati come belve grazie a Saturno",
		"Presi per i fondelli da Mercurio",
		"Ammaliati da Venere",
		"Intontiti da Marte",
		"Tranquillizzati dal transito di Venere",
		"Con l'animo reso inquieto da Saturno",
		"Grazie a Marte che passa e va",
		"Sotto l'influsso benefico di ben tre pianeti",
		"Incantati da Venere con il piercing all'ombelico",
		"Sopraffatti da Giove che ha  appena mangiato la pizza con le cipolle",
		"Buggerati da Urano che vi prometteva mari e monti",
		"Galvanizzati dalla presenza di Mercurio",
		"Ostacolati dal perfido Plutone",
		"Non godendo del favore di Giove",
		"Con l'aiuto di Saturno",
		"Con Urano che gira nel vostro segno seminando zizzania",
		"Con Mercurio si tiene ben lontano da voi",
		"Incantati da Venere e Giove in coppia sulla vespa",
		"Resi iracondi dal battagliero Marte",
		"Pacificati dalla presenza di Nettuno",
		"Completamente arrapati da una Venere in topless",
		"Con Marte dalla vostra parte",
		"Con Giove che fa il tifo per voi",
		"Resi frenetici da un Marte iper-attivo",
		"Con Saturno che sbuffa",
		"Mentre Venere si fa la ceretta",
		"Mentre Mercurio si fa un McCheese doppio",
		"Intrippati dalla presenza di Urano",
		"Appoggiati dalla benevolenza di Venere e Giove",
		"Nel frattempo che Giove si fa un pisolino",
		"Mentre Saturno e Giove giocano alla cavallina",
		"Mentre Urano e Plutone se ne fregano di voi",
		"Mentre Urano spia Venere dal buco della serratura",
		"Mentre Nettuno si fa le pippe",
		"Mentre Giove, Saturno e Urano fanno un pokerino",
		"Mentre Giove dorme",
		"Mentre Saturno guarda il Maurizio Costanzo Scio'",
		"Mentre Mercurio gioca al dottore con Venere",
		"Mentre Urano e Nettuno si fanno una briscola",
		"Quando Giove andra' in pensione",
		"Se Venere si distrae un attimo",
		"Capitasse mai che Marte decidesse di cambiare sesso",
		"Con Plutone che viaggia ben distante da voi",
		"Incalzati da Venere con il ciclo",
		"Adesso che Giove se n'e' andato",
		"Adesso che Saturno e' al gabinetto",
		"Nel frattempo che Marte guarda la Juve in tivvu'",
		"Nel frattempo che Venere fa all'amore con Giove"
	).build();

	private static final List<String> part2 = ImmutableList.<String>builder().add(
		"sara' difficile fermarvi, qualunque cosa abbiate deciso di fare. soprattutto se si trattera' di una gran cazzata",
		"dovrete sopportare seccature sul lavoro e incazzature a casa, ma per vostra moglie questo ed altro",
		"vi saranno serenita', incontri fortunati, aperture inaspettate, insomma due palle infinite",
		"sarete molto attivi nell'organizzare incontri coi parenti e amici, che vi ricopriranno di ingratitudine",
		"tenderete pericolosamene a sbandare in curva, a causa del forte vento fra Roncobilaccio e Barberino di Mugello",
		"vi troverete prepotentemente di fronte a un problema che dovreste affrontare con chiarezza",
		"vi sentirete stanchi, frastornati e sottoposti a polemiche stressanti",
		"mieterete successi in campo amoroso e/o economico, sia pure per poco tempo e in modo illusorio",
		"un imprevisto potrebbe scombinare fastidiosamente precedenti progetti, mandando tutto a puttane",
		"sarete bersagliati da pallottole di merda grosse come angurie, che pero' schiverete con grande agilita'",
		"cresceranno tensioni che mettono in luce problematiche irrisolte, fino alla rottura totale delle palle",
		"trascorrerete piacevoli momenti in amore o sul lavoro, o in tutte e due, o in nessuno dei due",
		"proverete una crescente insofferenza per tutto cio' che non rientra nel vostro ristretto ordine delle cose",
		"vi saranno evoluzioni di nuovi progetti che potrebbero portare a cambiamenti irrilevanti",
		"arrivera' una piccola sorpresa in amore, che rimettera' in discussione certe cose su cui speravate di passarla liscia",
		"sarete stimolati da novita' impreviste, che vi favoriranno la diuresi e l'eliminazione dell'acido urico in eccesso",
		"terrete sotto controllo la situazione, anche nei punti piu' difficili, dove di solito si annida lo sporco",
		"si concretizzera' l'evoluzione di nuovi progetti, che potrebbero cambiare",
		"sarete bersagliati da tensioni sgradevoli, a cui opporrete mutismo e rassegnazione",
		"sarete sostenuti dalla cintura elastica del dottor Gibaud, con il cotone sulla pelle",
		"farete progetti di rinnovamento della casa, nonostante il mutuo ventennale",
		"avrete una piccola eccitante sorpresa in amore, che potrebbe indurvi felicita'",
		"chiarirete le cose una volta per tutte, se possibile, il che e' improbabile",
		"darete il via a un nuovo progetto che promette successo e riuscita, ma l'ottimismo presto svanira'",
		"vi muoverete con grande dinamismo accompagnato da esitazioni ininfluenti",
		"sarete appoggiati da amici molto influenti, che pero' non muoveranno un dito",
		"sara' iperstimolata la creativita', il che consentira' di mettere molta carne al fuoco",
		"se sarete svelti potrete cogliere un'occasione molto fortunata ma disonesta",
		"subirete le lamentele di un partner che chiede piu' calma e riflessione",
		"cominceranno a prendere forma alcune idee relative a cambiamenti necessari anche se faticosi",
		"dovrete sostenere le tensioni suscitate da chi non gradisce i vostri progetti",
		"per voi sara' piu' dolce l'amore e piu' costruttivo il lavoro",
		"trascorrerete indimenticabili momenti nell'Agriturismo La Filanda, a San Giovanni in Persiceto (carte di credito: tutte)",
		"coglierete ogni occasione adatta per fare brevi viaggi o spostamenti",
		"vi concentrerete sul lavoro, settore in cui si sta consolidando un'idea interessante",
		"forse un progetto andra' in porto, ma forse no, adesso non e' io che posso sapere proprio tutto tutto tutto",
		"godrete di un piacevole equilibrio fra belle emozioni d'amore e positiva capacita' di concentrazione",
		"sarete pieni di concretezza e maturita' nell'affrontare l'eccesso di impegni",
		"risolverete seccature di lavoro in maniera non proprio brillante ne' elegante",
		"sarete assai vulnerabili, soprattutto nelle unghie dei piedi",
		"faticherete ad accettare cambiamenti non desiderati, soprattutto se accompagnati da insulti",
		"potreste infastidire il partner, che reclamera' piu' calma nella vita privata",
		"rifletterete a lungo su importanti cambiamenti, e vi verra' un gran mal di testa",
		"avrete importanti e interessanti riconoscimenti nel campo del lavoro e/o dell'amore",
		"comincerete a percepire una crescente insofferenza nei confronti dell'Elisir Rocchetta",
		"sarete costretti a chiarire una importante questione una volta per tutte, ricorrendo eventualmente al'omicidio",
		"dalle relazioni mondane scaturiranno incontri assai vantaggiosi ma che non porteranno a niente di concreto",
		"vi saranno gioni in cui non avrete voglia di fare assolutamente un cazzo",
		"sarete sostenuti da una bella energia costruttiva, che vi galvanizzera' interamente",
		"sarete costretti ad agire molto rapidamente senza troppo spazio alle esitazioni"
	).build();

	private static final List<String> part3 = ImmutableList.<String>builder().add(
		"andate sino al Largo Colombo: se passate dal 'Via!' ritirate le 20.000 lire.",
		"rimborso tassa sul reddito: ritirate 2.000 lire dalla Banca.",
		"dovete pagare un contributo di miglioria stradale: 4.000 lire per ogni casa, 10.000 lire per ogni albergo che possedete.",
		"e' il vostro compleanno: ogni giocatore vi regala 1.000 lire.",
		"andate alla stazione NORD: se passate dal 'Via!' ritirate le 20.000 lire.",
		"uscite gratis di prigione, se ci siete: potete conservare questo cartoncino sino al momento di servirvene (non si sa mai!) oppure venderlo.",
		"andate avanti fino al Parco della Vittoria.",
		"avete vinto il secondo premio a un concorso di bellezza: ritirate 1.000 lire.",
		"fate tre passi indietro (con tanti auguri!)",
		"andate fino al 'Via!'",
		"maturano le cedole delle vostre cartelle di rendita, ritirate 15.000 lire.",
		"scade il Vostro premio di assicurazione: pagate 5.000 lire.",
		"matrimonio in famiglia: spese impreviste 15.000 lire.",
		"pagate una multa di 1.000 lire, oppure prendete un cartoncino dagli 'imprevisti'.",
		"avete tutti i vostri stabili da riparare: pagare 2.500 lire per ogni casa e 10.000 lire per ogni albergo.",
		"siete creditore verso la Banca di 20.000 lire: ritiratele.",
		"versate 2.000 lire per beneficienza.",
		"avete venduto delle azioni: ricavate 5.000 lire.",
		"andate sino a 'Via Accademia': se passate dal 'Via!' ritirate le 20.000 lire.",
		"andate in prigione direttamente e senza passare dal 'Via!'",
		"avete vinto un terno al lotto: ritirate 10.000 lire.",
		"uscite gratis di prigione, se ci siete: potete conservare questo cartoncino sino al momento di servirvene (non si sa mai!) oppure venderlo.",
		"pagate il conto del Dottore: 5.000 lire.",
		"andate in prigione direttamente e senza passare dal 'Via!'",
		"ereditate da un lontano parente 10.000 lire.",
		"multa di 1.500 lire per aver guidato senza patente.",
		"avete perso una causa: pagate L. 10.000",
		"andate avanti sino al 'Via!'",
		"ritornate al Vicolo Corto",
		"la Banca vi paga gli interessi del vostro Conto Corrente: ritirate 5.000 lire.",
		"e' maturata la cedola delle vostre azioni: ritirate 2.500 lire.",
		"maturano le cedole delle vostre cartelle di rendita, ritirate 15.000 lire.",
		"scade il Vostro premio di assicurazione: pagate 5.000 lire.",
		"matrimonio in famiglia: spese impreviste 15.000 lire.",
		"pagate una multa di 1.000 lire, oppure prendete un cartoncino dagli 'imprevisti'.",
		"avete tutti i vostri stabili da riparare: pagare 2.500 lire per ogni casa e 10.000 lire per ogni albergo.",
		"siete creditore verso la Banca di 20.000 lire: ritiratele.",
		"versate 2.000 lire per beneficienza.",
		"avete venduto delle azioni: ricavate 5.000 lire.",
		"andate sino a 'Via Accademia': se passate dal 'Via!' ritirate le 20.000 lire.",
		"andate in prigione direttamente e senza passare dal 'Via!'",
		"avete vinto un terno al lotto: ritirate 10.000 lire.",
		"uscite gratis di prigione, se ci siete: potete conservare questo cartoncino sino al momento di servirvene (non si sa mai!) oppure venderlo.",
		"pagate il conto del Dottore: 5.000 lire.",
		"andate in prigione direttamente e senza passare dal 'Via!'",
		"ereditate da un lontano parente 10.000 lire.",
		"multa di 1.500 lire per aver guidato senza patente.",
		"avete perso una causa: pagate L. 10.000",
		"andate avanti sino al 'Via!'",
		"ritornate al Vicolo Corto"
	).build();

	private static final List<String> bullets = ImmutableList.<String>builder().add(
		"Amore",
		"Sesso",
		"Salute",
		"Viaggi",
		"Soldi",
		"Amici",
		"Lavoro",
		"Parcheggio"
	).build();

	private static final List<String> bulletContent = ImmutableList.<String>builder().add(
		"mavattinne, va'!",
		"miiiiiizeca!",
		"beddamatri!",
		"naaaaaahhhh...",
		"esticazzi.",
		"nunneppossibbile!",
		"yeppa, yeppa, yeppa!",
		"nononononono'!",
		"bah.",
		"una figata.",
		"lasuma perdi.",
		"oh, yeah.",
		"anvedi!",
		"ma mi faccia il piacere!",
		"evvai!",
		"mbeh...",
		"voio la mamma!",
		"orcocan!!",
		"ma 'ndo vai se la banana nun cellai?",
		"minchia!",
		"ehmmmmm...",
		"ninti.",
		"maddai!",
		"matusippazz!",
		"nuddu.",
		"nada de nada.",
		"vai lungo!",
		"ma vieni!",
		"oh, oh, oh, oh.",
		"achtung!",
		"matusippazz!",
		"che culo!",
		"ostrega!",
		"uaz! uaz! uaz!",
		"boh.",
		"yuk! yuk! yuk",
		"bi-bip!",
		"slurp!",
		"gnam!",
		"garwsch!",
		"mapperlacarita'!",
		"futtitinni.",
		"maronndokarmine!",
		"to-yeah!",
		"eh, beh...",
		"op, op, op!",
		"sgrunt!",
		"nun ci posso credere!",
		"eh, eh, eh...",
		"andale! andale! andale!"
	).build();

	@Inject
	HoroscopeCommandHandler(@Named("horoscopeCommand") String command) {
		super(command);
	}

	@Override
	public void handle(CommandLine commandLine) {

		StringBuilder response = new StringBuilder();
		response.append(part1.get(rand.nextInt(part1.size())));
		response.append(" ");
		response.append(part2.get(rand.nextInt(part2.size())));
		response.append("; intanto, ");
		response.append(part3.get(rand.nextInt(part3.size())));
		response.append(".");
		for (String bullet : bullets) {
			response.append("\n");
			response.append("*").append(bullet).append("*: ");
			response.append(bulletContent.get(rand.nextInt(bulletContent.size())));
		}

		SendMessage sendMessage = new SendMessage(commandLine.getChatId(), response.toString());
		sendMessage.enableMarkdown(true);

		messageDispatcher.doSendMessage(sendMessage, this.getClass());
	}


}

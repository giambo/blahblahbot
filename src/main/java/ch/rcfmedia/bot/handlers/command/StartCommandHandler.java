package ch.rcfmedia.bot.handlers.command;

import org.telegram.telegrambots.api.methods.send.SendMessage;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
public class StartCommandHandler extends BaseCommandHandler {

	@Inject
	StartCommandHandler(@Named("startCommand") String command) {
		super(command);
	}

	@Override
	public void handle(CommandLine commandLine) {
		SendMessage sendMessage = new SendMessage(commandLine.getChatId(), getStartMessage());
		sendMessage.enableMarkdown(true);

		messageDispatcher.doSendMessage(sendMessage, this.getClass());
	}

	private String getStartMessage() {
		return "*Statistiche:*\n" +
				"/stats 1h: Nell'ultima ora\n" +
				"/stats 24h: Nelle ultime 24 ore\n" +
				"/stats sempre: Da quando me lo ricordo\n" +
				"\n" +
				"*Oroscopo:*\n" +
				"/oroscopo" +
				"\n" +
				"*Consigli per il letto:*\n" +
				"/kamasutra" +
				"\n" +
				"*Bestemmia:*\n" +
				"/bestemmia" +
				"\n" +
				"*Monete virtuali:*\n" +
				"/btc _valuta_: Bitcoin in _valuta_ (3 lettere)\n" +
				"/eth _valuta_: Ethereum in _valuta_ (3 lettere)";
	}
}

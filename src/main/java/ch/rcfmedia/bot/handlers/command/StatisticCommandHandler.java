package ch.rcfmedia.bot.handlers.command;

import ch.rcfmedia.bot.persistence.Persistence;
import ch.rcfmedia.bot.statistics.StatisticOutputRow;
import ch.rcfmedia.bot.statistics.StatisticPeriod;
import com.google.common.collect.Lists;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class StatisticCommandHandler extends BaseCommandHandler {

	@Inject
	private	Persistence persistence;

	@Inject
	StatisticCommandHandler(@Named("statsCommand") String command) {
		super(command);
	}

	@Override
	public void handle(CommandLine commandLine) {
		String period = commandLine.getFirstArgument();
		StatisticPeriod statisticPeriod = StatisticPeriod.byUiText(period);
		if (statisticPeriod == null) {
			SendMessage sendMessage = getKeyboardForStats(commandLine.getChatId());
			messageDispatcher.doSendMessage(sendMessage, this.getClass());
			return;
		}
		SendMessage sendMessage = getStatsMessage(commandLine.getChatId(), statisticPeriod);

		messageDispatcher.doSendMessage(sendMessage, this.getClass());
	}

	private SendMessage getKeyboardForStats(Long chatId) {
		List<InlineKeyboardButton> buttonsRow = Lists.newArrayList();
		InlineKeyboardButton button;
		// sempre
		button = new InlineKeyboardButton();
		button.setText("Da sempre").setCallbackData(String.format("/stats %s", StatisticPeriod.BEGIN.getUiText()));
		buttonsRow.add(button);
		// 24h
		button = new InlineKeyboardButton();
		button.setText("Ultime 24 ore")
				.setCallbackData(String.format("/stats %s", StatisticPeriod.ONE_DAY.getUiText()));
		buttonsRow.add(button);
		// 1h
		button = new InlineKeyboardButton();
		button.setText("Ultima ora").setCallbackData(String.format("/stats %s", StatisticPeriod.ONE_HOUR.getUiText()));
		buttonsRow.add(button);

		// add it to the message
		InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
		markupInline.setKeyboard(Collections.singletonList(buttonsRow));

		SendMessage message = new SendMessage(chatId, "Numero di messaggi:");
		message.setReplyMarkup(markupInline);
		message.enableMarkdown(true);
		return message;
	}

	private SendMessage getStatsMessage(Long chatId, StatisticPeriod statisticPeriod) {
		List<StatisticOutputRow> stats = persistence.getStats(chatId, statisticPeriod);
		String statsFormatted = stats.stream().map(StatisticOutputRow::getStatRow).collect(Collectors.joining("\n"));
		String statsMessage = "Numero di messaggi:\n" + statsFormatted;
		SendMessage message = new SendMessage(chatId, statsMessage);
		message.enableMarkdown(true);
		return message;
	}
}

package ch.rcfmedia.bot.handlers.command.kamasutra;

import java.util.List;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class Content {

	private String title;
	private String subTitle = "";
	private List<String> description = Lists.newArrayList();

	public void setTitle(String title) {
		this.title = "*" + title + "*";
	}

	public boolean hasTitle() {
		return !Strings.isNullOrEmpty(title);
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = "_" + subTitle + "_";
	}

	public boolean hasSubTitle() {
		return !Strings.isNullOrEmpty(subTitle);
	}

	public void addDescription(String description, boolean isEmphatized) {
		if (isEmphatized) {
			this.description.add("_" + description + "_");
		} else {
			this.description.add(description);
		}
	}

	public boolean hasDescription() {
		return !description.isEmpty();
	}

	public String getAsMessage() {
		StringBuilder ret = new StringBuilder();
		ret.append(title).append("\n");

		if (!Strings.isNullOrEmpty(subTitle)) {
			ret.append(subTitle).append("\n");
		}

		ret.append("\n");

		description.forEach(ret::append);

		return ret.toString();
	}
}

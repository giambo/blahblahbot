package ch.rcfmedia.bot.handlers.command.kamasutra;

import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import com.google.common.base.Strings;

class DescriptionNodeHandler implements INodeHandler {

	@Override
	public void handle(Node node, Content content) {
		if (canHandle(node)) {
			if (node instanceof Element) {
				Element element = (Element) node;
				String description = element.text();
				content.addDescription(description, true);
			}
			if (node instanceof TextNode) {
				TextNode textNode = (TextNode) node;
				String description = textNode.text();
				content.addDescription(description, false);
			}
		}
	}

	private boolean canHandle(Node node) {
		if (node instanceof Element) {
			Element element = (Element) node;
			if (!"em".equalsIgnoreCase(element.tagName())) {
				return false;
			}
			String text = element.text().trim();
			if (text.startsWith("(") && text.endsWith(")")) {
				return false;
			}
			return true;
		}

		if (node instanceof TextNode) {
			TextNode textNode = (TextNode) node;
			String text = textNode.text().trim();
			return !Strings.isNullOrEmpty(text);
		}

		return false;
	}

}

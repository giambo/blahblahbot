package ch.rcfmedia.bot.handlers.command.kamasutra;

import org.jsoup.nodes.Node;

@FunctionalInterface
public interface INodeHandler {

	void handle(Node node, Content content);
	
}

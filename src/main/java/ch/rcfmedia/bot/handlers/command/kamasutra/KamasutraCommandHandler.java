package ch.rcfmedia.bot.handlers.command.kamasutra;

import ch.rcfmedia.bot.handlers.command.BaseCommandHandler;
import ch.rcfmedia.bot.handlers.command.CommandLine;
import com.google.common.collect.ImmutableList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.telegram.telegrambots.api.methods.send.SendMessage;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.Collection;

@Singleton
public class KamasutraCommandHandler extends BaseCommandHandler {

	private static final Log log = LogFactory.getLog(KamasutraCommandHandler.class);
	private static final String url = "http://www.polygen.org/it/grammatiche/rubriche/ita/kamasutra.grm";

	private final ImmutableList<INodeHandler> nodeHandlers;

	@Inject
	public KamasutraCommandHandler(@Named("kamasutraCommand") String command) {
		super(command);
		this.nodeHandlers = ImmutableList.of(
				new TitleNodeHandler(),
				new SubTitleNodeHandler(),
				new DescriptionNodeHandler()
		);
	}

	@Override
	public void handle(CommandLine commandLine) {
		SendMessage sendMessage = new SendMessage(commandLine.getChatId(), getContent());
		sendMessage.enableMarkdown(true);

		messageDispatcher.doSendMessage(sendMessage, this.getClass());
	}

	private String getContent() {
		Document document;
		try {
			document = Jsoup.connect(url).post();
		} catch (IOException e) {
			log.error("Error fetching url", e);
			return "Ooops ! Qualcosa e' andato storto ...";
		}
		Element root = document.select("div.generation").first();
		if (root == null) {
			log.error("Error getting div.generation");
			return "Ooops ! Qualcosa e' andato storto ...";
		}

		Content content = parseNodes(root.childNodes());
		return content.getAsMessage();
	}

	Content parseNodes(Collection<Node> nodes) {
		Content content = new Content();
		nodes.forEach(node -> nodeHandlers.forEach(handler -> handler.handle(node, content)));
		return content;
	}

}

package ch.rcfmedia.bot.handlers.command.kamasutra;

import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

class SubTitleNodeHandler implements INodeHandler {

	@Override
	public void handle(Node node, Content content) {
		if (canHandle(node, content)) {
			Element element = (Element) node;
			String subTitle = element.text().trim();
			content.setSubTitle(subTitle);
		}
	}

	private boolean canHandle(Node node, Content content) {
		if (content.hasSubTitle()) {
			// sub title already set
			return false;
		}
		if (node instanceof Element) {
			Element element = (Element) node;
			if (!"em".equalsIgnoreCase(element.tagName())) {
				return false;
			}
			String text = element.text().trim();
			return text.startsWith("(") && text.endsWith(")");
		}
		return false;
	}

}

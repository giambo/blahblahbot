package ch.rcfmedia.bot.handlers.command.kamasutra;

import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

class TitleNodeHandler implements INodeHandler {

	@Override
	public void handle(Node node, Content content) {
		if (canHandle(node, content)) {
			Element element = (Element) node;
			String title = element.text().trim();
			content.setTitle(title);
		}
	}

	private boolean canHandle(Node node, Content content) {
		if (content.hasTitle()) {
			// title already set
			return false;
		}
		if (node instanceof Element) {
			Element element = (Element) node;
			return "strong".equalsIgnoreCase(element.tagName());
		}
		return false;
	}

}

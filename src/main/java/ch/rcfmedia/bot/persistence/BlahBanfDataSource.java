package ch.rcfmedia.bot.persistence;

import ch.rcfmedia.bot.BlahBanfConfig;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

@Singleton
public class BlahBanfDataSource extends MysqlDataSource {

	private static final Log log = LogFactory.getLog(BlahBanfDataSource.class);

	private static final String createTableFile = "/sql/createTable.sql";

	@Inject
	public BlahBanfDataSource(BlahBanfConfig config) throws SQLException {
		setServerName(config.getHostName());
		setDatabaseName("blahBanf");
		setUser(config.getUserName());
		setPassword(config.getPassword());

		checkTable();
	}

	private void checkTable() throws SQLException {
		Connection conn = getConnection();
		ResultSet rs = conn.getMetaData().getTables(null, null, "stats", new String[] { "TABLE" });
		if (!rs.next()) {
			log.info("Creating table");
			InputStream createTable = getClass().getResourceAsStream(createTableFile);
			Scanner scanner = new Scanner(createTable).useDelimiter(";");
			while (scanner.hasNext()) {
				String table = scanner.next();
				PreparedStatement ps = conn.prepareStatement(table);
				ps.execute();
			}
		}
	}
}

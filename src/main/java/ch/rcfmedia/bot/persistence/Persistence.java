package ch.rcfmedia.bot.persistence;

import ch.rcfmedia.bot.statistics.StatisticOutputRow;
import ch.rcfmedia.bot.statistics.StatisticPeriod;
import ch.rcfmedia.bot.statistics.StatisticsInputRow;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class Persistence {

	@Inject
	Statistics statistic;

	public void updateStats(StatisticsInputRow statisticUpdateRow) {
		statistic.updateStats(statisticUpdateRow);
	}

	public List<StatisticOutputRow> getStats(Long chatId, StatisticPeriod statisticPeriod) {
		return statistic.getStats(chatId, statisticPeriod);
	}
}

package ch.rcfmedia.bot.persistence;

import ch.rcfmedia.bot.statistics.StatisticOutputRow;
import ch.rcfmedia.bot.statistics.StatisticPeriod;
import ch.rcfmedia.bot.statistics.StatisticsInputRow;
import com.google.common.collect.ImmutableList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Singleton
public class Statistics {

	private static final String SQL_INSERT =
			"INSERT INTO stats" + "(chat_id, user_id, user_name, first_name, message_length, update_date)"
					+ "VALUES (?, ?, ?, ?, ?, ?)";

	private static final String SQL_GET = "SELECT user_id, user_name, first_name, count(user_id) AS messages FROM stats "
			+ "WHERE chat_id = ? AND update_date > ? " + "GROUP BY (user_id) " + "ORDER BY messages DESC";

	private static final Log log = LogFactory.getLog(Statistics.class);

	@Inject
	DataSource dataSource;

	public void updateStats(StatisticsInputRow statisticUpdateRow) {
		try (PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL_INSERT)) {
			int index = 1;
			ps.setLong(index++, statisticUpdateRow.getChatId());
			ps.setInt(index++, statisticUpdateRow.getUserId());
			ps.setString(index++, statisticUpdateRow.getUserName());
			ps.setString(index++, statisticUpdateRow.getFirstName());
			ps.setInt(index++, statisticUpdateRow.getMessageLength());
			ps.setTimestamp(index++, Timestamp.valueOf(LocalDateTime.now()));
			ps.execute();
			log.info("Update stats: " + statisticUpdateRow.toString());
		} catch (SQLException e) {
			log.error("Cannot insert " + statisticUpdateRow.toString(), e);
		}
	}

	List<StatisticOutputRow> getStats(Long chatId, StatisticPeriod statisticPeriod) {
		ImmutableList.Builder<StatisticOutputRow> messagesPerUser = ImmutableList.builder();
		LocalDateTime startingDate;
		switch (statisticPeriod) {
			case ONE_DAY:
				startingDate = LocalDateTime.now().minusDays(1);
				break;
			case ONE_HOUR:
				startingDate = LocalDateTime.now().minusHours(1);
				break;
			default:
				startingDate = LocalDateTime.MIN;
		}
		try (PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL_GET)) {
			int index = 1;
			ps.setLong(index++, chatId);
			ps.setTimestamp(index++, Timestamp.valueOf(startingDate));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Integer userId = rs.getInt("user_id");
				String userName = rs.getString("user_name");
				String firstName = rs.getString("first_name");
				Integer messages = rs.getInt("messages");
				messagesPerUser.add(new StatisticOutputRow(userId, userName, firstName, messages));
			}
		} catch (SQLException e) {
			String errorMsg = "Cannot get stats for chatId=%s";
			log.error(String.format(errorMsg, chatId), e);
		}
		return messagesPerUser.build();
	}

}

package ch.rcfmedia.bot.statistics;

import com.google.common.base.Strings;

public class StatisticInputParser {
	
	public static boolean hasStatsCommand(String inputText) {
		if (Strings.isNullOrEmpty(inputText)) {
			return false;
		}
		return inputText.toLowerCase().startsWith("/stats");
	}
	
	public static StatisticPeriod getStatsPeriod(String inputText) {
		if (Strings.isNullOrEmpty(inputText)) {
			return null;
		}
		if (!hasStatsCommand(inputText)) {
			return null;
		}
		String[] parts = inputText.split("\\s+");
		if (parts.length > 1) {
			String uiText = parts[1];
			return StatisticPeriod.byUiText(uiText);
		}
		return null;
	}
}

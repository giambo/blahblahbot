package ch.rcfmedia.bot.statistics;

import com.google.common.base.Strings;

public class StatisticOutputRow {

	private static final String USER_MENTION = "[%s](tg://user?id=%s)";

	private Integer userId;
	private String user;
	private Integer messages;

	public StatisticOutputRow(Integer userId, String userName, String firstName, Integer messages) {
		this.userId = userId;
		if (!Strings.isNullOrEmpty(firstName)) {
			user = firstName;
		} else if (!Strings.isNullOrEmpty(userName)) {
			user = userName;
		} else {
			user = Integer.toString(userId);
		}
		this.messages = messages;
	}

	public String getStatRow() {
		return getUserMention() + " : " + messages;
	}

	private String getUserMention() {
		return String.format(USER_MENTION, user, userId);
	}

}

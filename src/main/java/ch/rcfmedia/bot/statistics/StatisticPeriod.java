package ch.rcfmedia.bot.statistics;

import com.google.common.base.Strings;

public enum StatisticPeriod {

	ONE_HOUR("1h"), ONE_DAY("24h"), BEGIN("sempre");

	private String uiText;

	StatisticPeriod(String uiText) {
		this.uiText = uiText;
	}

	public String getUiText() {
		return uiText;
	}

	public static StatisticPeriod byUiText(String uiText) {
		if (Strings.isNullOrEmpty(uiText)) {
			return null;
		}

		String statPeriodText = uiText.trim();
		if (statPeriodText.length() == 0) {
			return null;
		}

		for (StatisticPeriod statPeriod : StatisticPeriod.values()) {
			if (statPeriod.uiText.compareToIgnoreCase(statPeriodText) == 0) {
				return statPeriod;
			}
		}

		return null;
	}
}

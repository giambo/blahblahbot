package ch.rcfmedia.bot.statistics;

import org.telegram.telegrambots.api.objects.Message;

public class StatisticsInputRow {

	private Long chatId;
	private Integer userId;
	private String userName;
	private String firstName;
	private Integer messageLength;

	public StatisticsInputRow(Message message) {
		chatId = message.getChatId();
		userId = message.getFrom().getId();
		userName = message.getFrom().getUserName();
		firstName = message.getFrom().getFirstName();
		messageLength = message.getText().length();
	}

	public String toString() {
		return String.format("chatId=%s userId=%s userName=%s firstName=%s messageLength=%s",
				chatId, userId, userName, firstName, messageLength
		);
	}

	public Long getChatId() {
		return chatId;
	}

	public Integer getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public Integer getMessageLength() {
		return messageLength;
	}
}

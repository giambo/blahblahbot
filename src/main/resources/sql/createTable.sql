CREATE TABLE IF NOT EXISTS stats(
  id INT NOT NULL AUTO_INCREMENT,
  chat_id BIGINT NOT NULL,
  user_id INT NOT NULL,
  user_name VARCHAR(255) NULL,
  first_name VARCHAR(255) NULL,
  message_length INT NOT NULL,
  update_date TIMESTAMP NOT NULL,
  PRIMARY KEY (id)
)
;
package ch.rcfmedia.bot;

import ch.rcfmedia.bot.handlers.ICallbackHandler;
import ch.rcfmedia.bot.handlers.IMessageHandler;
import ch.rcfmedia.bot.handlers.command.BaseCommandHandler;
import ch.rcfmedia.bot.handlers.command.CommandLine;
import ch.rcfmedia.bot.persistence.Persistence;
import ch.rcfmedia.bot.persistence.Statistics;
import ch.rcfmedia.bot.statistics.StatisticsInputRow;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;

public class BlahBanfBotTest {

	private BlahBanfBot blahBanfBot;
	private Statistics statistics;
	private IMessageDispatcher messageDispatcher;
	private ICallbackHandler callbackHandlerOK;
	private ICallbackHandler callbackHandlerNOK;
	private IMessageHandler messageHandlerOK;
	private IMessageHandler messageHandlerNOK;
	private BaseCommandHandler commandHandlerOK;
	private BaseCommandHandler commandHandlerNOK;

	private static ObjectMapper objectMapper;

	@BeforeClass
	public static void init() {
		objectMapper = new ObjectMapper();
	}

	@Before
	public void setup() {

		statistics = Mockito.mock(Statistics.class);
		messageDispatcher = Mockito.mock(IMessageDispatcher.class);

		callbackHandlerOK = Mockito.spy(new TestCallbackHandler("/command"));
		callbackHandlerNOK = Mockito.spy(new TestCallbackHandler("/quack"));

		messageHandlerOK = Mockito.spy(new TestMessageHandler("text"));
		messageHandlerNOK = Mockito.spy(new TestMessageHandler("quack"));

		commandHandlerOK = Mockito.spy(new TestCommandHandler("/command"));
		commandHandlerNOK = Mockito.spy(new TestCommandHandler("/quack"));

		Injector injector = Guice.createInjector((Binder binder) -> {
			binder.bind(String.class).annotatedWith(Names.named("botName")).toInstance("TestBot");
			binder.bind(Persistence.class);
			binder.bind(Statistics.class).toInstance(statistics);
			binder.bind(IMessageDispatcher.class).toInstance(messageDispatcher);
			binder.bind(DataSource.class).toInstance(Mockito.mock(DataSource.class));
			binder.bind(BlahBanfConfig.class).toInstance(Mockito.mock(BlahBanfConfig.class));
			binder.bind(BlahBanfBot.class);

			Multibinder<ICallbackHandler> cBinder = Multibinder.newSetBinder(binder, ICallbackHandler.class);
			cBinder.addBinding().toInstance(callbackHandlerOK);
			cBinder.addBinding().toInstance(callbackHandlerNOK);

			Multibinder<IMessageHandler> mBinder = Multibinder.newSetBinder(binder, IMessageHandler.class);
			mBinder.addBinding().toInstance(messageHandlerOK);
			mBinder.addBinding().toInstance(messageHandlerNOK);
			mBinder.addBinding().toInstance(commandHandlerOK);
			mBinder.addBinding().toInstance(commandHandlerNOK);
		});
		blahBanfBot = injector.getInstance(BlahBanfBot.class);
	}

	@Test
	public void test_callbackHandlers() throws IOException {
		Assert.assertNotNull(blahBanfBot);
		Update update = createUpdateCallback();
		ArgumentCaptor<CallbackQuery> callbackQuery;

		blahBanfBot.onUpdateReceived(update);

		callbackQuery = ArgumentCaptor.forClass(CallbackQuery.class);
		Mockito.verify(callbackHandlerOK, Mockito.times(1)).handle(callbackQuery.capture());
		callbackQuery = ArgumentCaptor.forClass(CallbackQuery.class);
		Mockito.verify(callbackHandlerNOK, Mockito.times(0)).handle(callbackQuery.capture());
	}

	@Test
	public void test_messageHandlers() throws IOException {
		Assert.assertNotNull(blahBanfBot);
		Update update = createUpdateMessage();
		ArgumentCaptor<Message> message;

		blahBanfBot.onUpdateReceived(update);

		message = ArgumentCaptor.forClass(Message.class);
		Mockito.verify(messageHandlerOK, Mockito.times(1)).handle(message.capture());
		message = ArgumentCaptor.forClass(Message.class);
		Mockito.verify(messageHandlerNOK, Mockito.times(0)).handle(message.capture());

		ArgumentCaptor<StatisticsInputRow> statisticsInputRow = ArgumentCaptor.forClass(StatisticsInputRow.class);
		Mockito.verify(statistics, Mockito.times(1)).updateStats(statisticsInputRow.capture());
		StatisticsInputRow value = statisticsInputRow.getValue();
		Assert.assertEquals(value.getChatId().longValue(), 42L);
		Assert.assertEquals(value.getUserId().intValue(), 111);
	}


	@Test
	public void test_commandHandlers() throws IOException {
		Assert.assertNotNull(blahBanfBot);
		Update update = createUpdateCommand();
		ArgumentCaptor<CommandLine> commandLine;

		blahBanfBot.onUpdateReceived(update);

		commandLine = ArgumentCaptor.forClass(CommandLine.class);
		Mockito.verify(commandHandlerOK, Mockito.times(1)).handle(commandLine.capture());
		commandLine = ArgumentCaptor.forClass(CommandLine.class);
		Mockito.verify(commandHandlerNOK, Mockito.times(0)).handle(commandLine.capture());

		ArgumentCaptor<StatisticsInputRow> statisticsInputRow = ArgumentCaptor.forClass(StatisticsInputRow.class);
		Mockito.verify(statistics, Mockito.times(0)).updateStats(statisticsInputRow.capture());
	}

	private Update createUpdateCallback() throws IOException {
		InputStream update = getClass().getResourceAsStream("/update_callback.json");
		return new ObjectMapper().readValue(update, Update.class);
	}

	private Update createUpdateMessage() throws IOException {
		InputStream message = getClass().getResourceAsStream("/update_message.json");
		return objectMapper.readValue(message, Update.class);
	}

	private Update createUpdateCommand() throws IOException {
		InputStream message = getClass().getResourceAsStream("/update_command.json");
		return objectMapper.readValue(message, Update.class);
	}

	private static class TestCallbackHandler implements ICallbackHandler {
		private final String command;

		TestCallbackHandler(String command) {
			this.command = command;
		}

		@Override
		public boolean canHandle(CallbackQuery callbackQuery) {
			return command.equals(callbackQuery.getData());
		}

		@Override
		public void handle(CallbackQuery callbackQuery) {
		}
	}

	private static class TestMessageHandler implements IMessageHandler {
		private final String text;

		TestMessageHandler(String text) {
			this.text = text;
		}

		@Override
		public boolean canHandle(Message message) {
			return message.getText().startsWith(text);
		}

		@Override
		public void handle(Message message) {
		}
	}

	private class TestCommandHandler extends BaseCommandHandler {

		TestCommandHandler(String command) {
			super(command);
		}

		@Override
		public void handle(CommandLine commandLine) {

		}
	}

}

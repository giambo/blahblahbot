package ch.rcfmedia.bot.handlers.command;

import ch.rcfmedia.bot.IMessageDispatcher;
import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import javax.inject.Inject;
import javax.inject.Named;

import static org.junit.Assert.*;

public class BaseCommandHandlerTest {

	private static BaseCommandHandlerTestImplementation baseArgumentCommandHandlerTest;
	private static CommandLineParser commandLineParser;

	@BeforeClass
	public static void init() {
		Injector injector = Guice.createInjector((Binder binder) -> {
			binder.bind(BaseCommandHandlerTestImplementation.class);
			binder.bind(IMessageDispatcher.class).toInstance(Mockito.mock(IMessageDispatcher.class));
			binder.bind(String.class).annotatedWith(Names.named("botName")).toInstance("TestBot");
			binder.bind(String.class).annotatedWith(Names.named("testCommand")).toInstance("/command");
		});

		baseArgumentCommandHandlerTest = injector.getInstance(BaseCommandHandlerTestImplementation.class);

		commandLineParser = new CommandLineParser();
		commandLineParser.botName = "TestBot";
	}

	@Test
	public void testCanHandle() {
		assertFalse(baseArgumentCommandHandlerTest.canHandle(getCommandLine(null)));
		assertFalse(baseArgumentCommandHandlerTest.canHandle(getCommandLine("")));
		assertFalse(baseArgumentCommandHandlerTest.canHandle(getCommandLine("command")));
		assertFalse(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/commandXYZ")));

		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/command")));
		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/ComMAnD")));
		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine(" /command")));
		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/command ")));
		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/command arg")));
		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/command arg1 arg2")));
	}

	@Test
	public void testCanHandle_botname() {
		assertFalse(baseArgumentCommandHandlerTest.canHandle(getCommandLine(null)));
		assertFalse(baseArgumentCommandHandlerTest.canHandle(getCommandLine("@TestBot")));
		assertFalse(baseArgumentCommandHandlerTest.canHandle(getCommandLine("command@TestBot")));
		assertFalse(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/commandXYZ@TestBot")));

		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/command@TestBot")));
		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine(" /command@TestBot")));
		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/command@TestBot ")));
		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/ComMAnD@TestBot")));
		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/command@TestBot arg")));
		assertTrue(baseArgumentCommandHandlerTest.canHandle(getCommandLine("/command@TestBot arg1 arg2")));
	}

	@Test
	public void testGetArgument() {
		assertNull(getCommandLine(null).getFirstArgument());
		assertNull(getCommandLine("").getFirstArgument());
		assertNull(getCommandLine("command").getFirstArgument());
		assertNull(getCommandLine(" /command").getFirstArgument());
		assertNull(getCommandLine("/commandXYZ").getFirstArgument());

		assertNull(getCommandLine("/command").getFirstArgument());
		assertNull(getCommandLine("/command ").getFirstArgument());
		assertEquals("arg", getCommandLine("/command arg").getFirstArgument());
		assertEquals("arg", getCommandLine("/command     arg   ").getFirstArgument());
		assertEquals("arg1", getCommandLine("/command arg1 arg2").getFirstArgument());
		assertEquals("arg2", getCommandLine("/command arg1 arg2").getSecondArgument());
		assertEquals("arg1", getCommandLine("/command     arg1    arg2   ").getFirstArgument());
		assertEquals("arg2", getCommandLine("/command     arg1    arg2   ").getSecondArgument());
	}

	@Test
	public void testGetArgument_botname() {
		assertNull(getCommandLine(null).getFirstArgument());
		assertNull(getCommandLine("@TestBot").getFirstArgument());
		assertNull(getCommandLine("command@TestBot").getFirstArgument());
		assertNull(getCommandLine(" /command@TestBot").getFirstArgument());
		assertNull(getCommandLine("/commandXYZ@TestBot").getFirstArgument());

		assertNull(getCommandLine("/command@TestBot").getFirstArgument());
		assertNull(getCommandLine("/command@TestBot ").getFirstArgument());
		assertEquals("arg", getCommandLine("/command@TestBot arg").getFirstArgument());
		assertEquals("arg", getCommandLine("/command@TestBot     arg   ").getFirstArgument());
		assertEquals("arg1", getCommandLine("/command arg1 arg2").getFirstArgument());
		assertEquals("arg2", getCommandLine("/command arg1 arg2").getSecondArgument());
		assertEquals("arg1", getCommandLine("/command@TestBot     arg1    arg2   ").getFirstArgument());
		assertEquals("arg2", getCommandLine("/command@TestBot     arg1    arg2   ").getSecondArgument());
	}

	private CommandLine getCommandLine(String text) {
		return commandLineParser.parse(text, 42L);
	}

	private static class BaseCommandHandlerTestImplementation extends BaseCommandHandler {

		@Inject
		BaseCommandHandlerTestImplementation(@Named("testCommand") String command) {
			super(command);
		}

		@Override
		public void handle(CommandLine commandLine) {
			// ignore
		}
	}
}

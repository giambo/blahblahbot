package ch.rcfmedia.bot.handlers.command;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CommandLineParserTest {

	private static final String botName = "TestBotName";

	private CommandLineParser commandLineParser;

	@Before
	public void setup() {
		commandLineParser = new CommandLineParser();
		commandLineParser.botName = botName;
	}

	@Test
	public void testOnlyCommand() {
		CommandLine commandLine = commandLineParser.parse(null, 42L);
		assertNull(commandLine.getCommand());
		assertNull(commandLine.getArguments());

		commandLine = commandLineParser.parse("", 42L);
		assertNull(commandLine.getCommand());
		assertNull(commandLine.getArguments());

		commandLine = commandLineParser.parse("xxxx", 42L);
		assertNull(commandLine.getCommand());
		assertNull(commandLine.getArguments());

		commandLine = commandLineParser.parse("/xxxx", 42L);
		assertEquals(commandLine.getCommand(), "/xxxx");
		assertNull(commandLine.getArguments());
	}

	@Test
	public void testCommandAndBotName() {
		CommandLine commandLine = commandLineParser.parse("/xxxx@InvalidBotName", 42L);
		assertEquals(commandLine.getCommand(), "/xxxx@InvalidBotName");
		assertNull(commandLine.getArguments());

		commandLine = commandLineParser.parse("/xxxx@" + botName, 42L);
		assertEquals(commandLine.getCommand(), "/xxxx");
		assertNull(commandLine.getArguments());
	}

	@Test
	public void testOnlyCommandAndArguments() {
		CommandLine commandLine = commandLineParser.parse("xxxx a bb ccc", 42L);
		assertNull(commandLine.getCommand());
		assertNull(commandLine.getArguments());

		commandLine = commandLineParser.parse("/xxxx a bb ccc", 42L);
		assertEquals(commandLine.getCommand(), "/xxxx");
		assertEquals(commandLine.getArguments().length, 3);
		assertEquals(commandLine.getArguments()[0], "a");
		assertEquals(commandLine.getArguments()[1], "bb");
		assertEquals(commandLine.getArguments()[2], "ccc");
	}


	@Test
	public void testCommandAndBotNameArguments() {
		CommandLine commandLine = commandLineParser.parse("/xxxx@InvalidBotName a bb ccc", 42L);
		assertEquals(commandLine.getCommand(), "/xxxx@InvalidBotName");
		assertEquals(commandLine.getArguments().length, 3);
		assertEquals(commandLine.getArguments()[0], "a");
		assertEquals(commandLine.getArguments()[1], "bb");
		assertEquals(commandLine.getArguments()[2], "ccc");

		commandLine = commandLineParser.parse("/xxxx@" + botName + " a bb ccc", 42L);
		assertEquals(commandLine.getCommand(), "/xxxx");
		assertEquals(commandLine.getArguments().length, 3);
		assertEquals(commandLine.getArguments()[0], "a");
		assertEquals(commandLine.getArguments()[1], "bb");
		assertEquals(commandLine.getArguments()[2], "ccc");
	}

	@Test
	public void testWithManySpaces() {
		CommandLine commandLine = commandLineParser.parse("  /xxxx@" + botName + "   a    bb    ccc    ", 42L);
		assertEquals(commandLine.getCommand(), "/xxxx");
		assertEquals(commandLine.getArguments().length, 3);
		assertEquals(commandLine.getArguments()[0], "a");
		assertEquals(commandLine.getArguments()[1], "bb");
		assertEquals(commandLine.getArguments()[2], "ccc");
	}
}

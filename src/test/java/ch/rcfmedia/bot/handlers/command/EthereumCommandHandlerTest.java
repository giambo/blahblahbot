package ch.rcfmedia.bot.handlers.command;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.telegram.telegrambots.api.methods.send.SendMessage;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;

import ch.rcfmedia.bot.IMessageDispatcher;

public class EthereumCommandHandlerTest {

	private EthereumCommandHandler handler;
	private CommandLineParser commandLineParser;
	private IMessageDispatcher messageDispatcher;

	@Before
	public void init() {
		Injector injector = Guice.createInjector((Binder binder) -> {
			binder.bind(CommandLineParser.class);
			binder.bind(EthereumCommandHandler.class).toInstance(Mockito.spy(new EthereumCommandHandler("/eth")));
			binder.bind(IMessageDispatcher.class).toInstance(Mockito.mock(IMessageDispatcher.class));
			binder.bind(String.class).annotatedWith(Names.named("botName")).toInstance("TestBot");
			binder.bind(String.class).annotatedWith(Names.named("ethCommand")).toInstance("/eth");
		});
		handler = injector.getInstance(EthereumCommandHandler.class);
		commandLineParser = injector.getInstance(CommandLineParser.class);
		messageDispatcher = injector.getInstance(IMessageDispatcher.class);
	}

	@Test
	public void testCanHandle() {
		Assert.assertEquals("ETH", handler.getFromCurrency());
		Assert.assertTrue(handler.canHandle(getCommandLine("/eth USD")));
		Assert.assertTrue(handler.canHandle(getCommandLine("/ETH Chf")));
		Assert.assertTrue(handler.canHandle(getCommandLine("/eth@TeSTboT eur")));
		Assert.assertTrue(handler.canHandle(getCommandLine("/eth@TestBot EuR")));
	}

	@Test
	public void testHandle() {
		ArgumentCaptor<SendMessage> captor = ArgumentCaptor.forClass(SendMessage.class);
		handler.handle(getCommandLine("/eth@TestBot eur"));

		Mockito.verify(messageDispatcher).doSendMessage(captor.capture(), Mockito.any(Class.class));
		SendMessage reply = captor.getValue();
		Assert.assertNotNull(reply);
		Assert.assertTrue(reply.getText().startsWith("Un ETH vale"));
		Assert.assertTrue(reply.getText().endsWith("EUR"));
	}

	@Test
	public void testHandle_noBotName() {
		ArgumentCaptor<SendMessage> captor = ArgumentCaptor.forClass(SendMessage.class);

		handler.handle(getCommandLine("/eth eur"));

		Mockito.verify(messageDispatcher).doSendMessage(captor.capture(), Mockito.any(Class.class));
		SendMessage reply = captor.getValue();
		Assert.assertNotNull(reply);
		Assert.assertTrue(reply.getText().startsWith("Un ETH vale"));
		Assert.assertTrue(reply.getText().endsWith("EUR"));
	}

	@Test
	public void testHandle_botNameUppercase() {
		ArgumentCaptor<SendMessage> captor = ArgumentCaptor.forClass(SendMessage.class);

		handler.handle(getCommandLine("/eth@TESTBOT ChF"));

		Mockito.verify(messageDispatcher).doSendMessage(captor.capture(), Mockito.any(Class.class));
		SendMessage reply = captor.getValue();
		Assert.assertNotNull(reply);
		Assert.assertTrue(reply.getText().startsWith("Un ETH vale"));
		Assert.assertTrue(reply.getText().endsWith("CHF"));
	}

	private CommandLine getCommandLine(String text) {
		return commandLineParser.parse(text, 42L);
	}

}

package ch.rcfmedia.bot.handlers.command.kamasutra;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public class KamasutraCommandHandlerTest {

	private static final String EXAMPLE_FILE_FULL = "full.html";
	private static final String EXAMPLE_FILE_SIMPLE = "simple.html";
	private static final String EXAMPLE_FILE_SUBTITLE = "subtitle.html";

	private KamasutraCommandHandler kamasutraCommandHandler;

	@Before
	public void init() {
		kamasutraCommandHandler = new KamasutraCommandHandler("/dummy");
	}

	@Test
	public void testParseDocument_full() throws IOException {
		Document document = getDocument(EXAMPLE_FILE_FULL);
		Element root = getRoot(document);

		Content content = kamasutraCommandHandler.parseNodes(root.childNodes());

		Assert.assertEquals(content.getAsMessage(), "*#315 - Posizione Del Bastone Odoroso*\n"
				+ "_( Versione alternativa della Tenaglia Sognante )_\n\n"
				+ " Facendo leva sul dito, il maschio tiene sollevata la propria compagna per il clitoride e, mentre "
				+ "lei gli sussurra _\"...si'!!! dimmi che sono una mucca pazza...\"_, lui le gratta con dolcezza il "
				+ "gomito destro. Una volta completati i preliminari finalmente lui la stimola sessualmente. ");
	}

	@Test
	public void testParseDocument_simple() throws IOException {
		Document document = getDocument(EXAMPLE_FILE_SIMPLE);
		Element root = getRoot(document);

		Content content = kamasutraCommandHandler.parseNodes(root.childNodes());

		Assert.assertEquals(content.getAsMessage(), "*#298 - Posizione Del Cavallo Giocoso*\n\n"
				+ " La donna serra con le mani i fianchi del proprio maschio. Nel frattempo lui guarda dal buco della "
				+ "serratura. Una volta che e' stato raggiunto il giusto grado di lubrificazione finalmente i due "
				+ "suini si mettono a limonare come due ricci indemoniati. ");
	}

	@Test
	public void testParseDocument_subtitle() throws IOException {
		Document document = getDocument(EXAMPLE_FILE_SUBTITLE);
		Element root = getRoot(document);

		Content content = kamasutraCommandHandler.parseNodes(root.childNodes());

		Assert.assertEquals(content.getAsMessage(), "*#5 - Posizione Del Ruscello Oleoso*\n"
				+ "_( E' una versione alternativa del Pelliciaio Oleoso )_\n\n"
				+ " Inarcando le sopracciglia, la partner stringe forte tra le gambe i fianchi del proprio uomo, "
				+ "mentre lui guida a tutta velocita'. Una volta completati i noiosi preliminari finalmente i due "
				+ "partner scopano insieme serenamente. ");
	}

	private Document getDocument(String resourceName) throws IOException {
		Assert.assertNotNull(resourceName);
		InputStream kamasutraExample = getClass().getResourceAsStream("/kamasutraExamples/" + resourceName);
		Assert.assertNotNull(kamasutraExample);
		Document document = Jsoup.parse(kamasutraExample, Charset.defaultCharset().name(), "http://localhost");
		Assert.assertNotNull(document);
		return document;
	}

	private Element getRoot(Document document) {
		Element root = document.select("div.generation").first();
		Assert.assertNotNull(root);
		return root;
	}

}
